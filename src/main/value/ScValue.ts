/*-
 * #%L
 * abi-client-ts
 * %%
 * Copyright (C) 2022 Partisia Blockchain Foundation
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */
import { TypeIndex } from "../types/Abi";
import { ScValueEnum } from "./ScValueEnum";
import { ScValueStruct } from "./ScValueStruct";
import { ScValueVector } from "./ScValueVector";
import { ScValueSet } from "./ScValueSet";
import { ScValueOption } from "./ScValueOption";
import { ScValueMap } from "./ScValueMap";
import { ScValueAddress } from "./ScValueAddress";
import BN from "bn.js";

export abstract class ScValue {
  abstract getType(): TypeIndex;

  public structValue(): ScValueStruct {
    throw new Error("Cannot read Struct for current type");
  }

  public vecValue(): ScValueVector {
    throw new Error("Cannot read Vector for current type");
  }

  public setValue(): ScValueSet {
    throw new Error("Cannot read Set for current type");
  }

  public optionValue(): ScValueOption {
    throw new Error("Cannot read Option for current type");
  }

  public addressValue(): ScValueAddress {
    throw new Error("Cannot read Address for current type");
  }

  public mapValue(): ScValueMap {
    throw new Error("Cannot read Map for current type");
  }

  public enumValue(): ScValueEnum {
    throw new Error("Cannot read Enum for current type");
  }

  public sizedByteArrayValue(): Buffer {
    throw new Error("Cannot read SizedByteArray for current type");
  }

  public boolValue(): boolean {
    throw new Error("Cannot read bool for current type");
  }

  public stringValue(): string {
    throw new Error("Cannot read String for current type");
  }

  public asNumber(): number {
    throw new Error("Cannot read number for current type");
  }

  public asBN(): BN {
    throw new Error("Cannot read BN for current type");
  }
}
